package itunes

import (
	"context"
	"encoding/json"
)

// GetFeedURLService get feedUrl info
type GetFeedURLService struct {
	c   *Client
	ids []string
}

// Do send request
func (s *GetFeedURLService) Do(ctx context.Context, opts ...RequestOption) (res FeedURLStruct, err error) {
	r := &request{
		method:   "GET",
		endpoint: "/lookup",
	}
	for _, id := range s.ids {

		r.addParam("id", id)
	}

	data, err := s.c.callAPI(ctx, r, opts...)
	if err != nil {
		return FeedURLStruct{}, err
	}

	err = json.Unmarshal(data, &res)
	if err != nil {
		return FeedURLStruct{}, err
	}
	return res, nil
}

// FeedURLStruct define FeedURLStruct
type FeedURLStruct struct {
	ResultCount int                  `json:"resultCount"`
	Results     []ResultsInnerStruct `json:"results"`
}

// ResultsInnerStruct
type ResultsInnerStruct struct {
	FeedUrl string `json:"feedUrl"`
}

// IDs set IDs
func (s *GetFeedURLService) IDs(IDs []string) *GetFeedURLService {
	s.ids = IDs
	return s
}
