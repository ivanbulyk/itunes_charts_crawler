package itunes

// podcast RSS feed

import (
	"context"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"strings"
)

// GetPodcastRSSFeedService get GetPodcastRSSFeed info
type GetPodcastRSSFeedService struct {
	c       *Client
	feedUrl string
}

// Do send request
func (s *GetPodcastRSSFeedService) Do(ctx context.Context, opts ...RequestOption) (res Podcast, err error) {
	r := &request{
		method:   "GET",
		endpoint: s.feedUrl,
	}

	data, err := s.c.callAPI(ctx, r, opts...)
	if err != nil {
		return Podcast{}, err
	}

	if strings.Contains(httpHeader["Content-Type"][0], "xml") {

		// Unmarshal takes a []byte and fills the rss struct with the values found in the xmlFile
		rss := Rss{}
		err = xml.Unmarshal(data, &rss)
		if err != nil {
			fmt.Printf("error: %v", err)
			return
		}
		fmt.Println("Rss version: " + rss.Version)

		res.Name = rss.Channel.Title
		res.Description = rss.Channel.Description
		episode := Episode{}

		for _, item := range rss.Channel.Items {
			episode.Name = item.Title
			episode.Description = item.Description
			episode.URL = item.Link
			episode.Duration = item.Duration
			res.ListOfEpisodes = append(res.ListOfEpisodes, episode)

		}
	} else {
		err = json.Unmarshal(data, &res)
		if err != nil {
			return Podcast{}, err
		}
	}

	return res, nil
}

type Rss struct {
	XMLName     xml.Name `xml:"rss"`
	Version     string   `xml:"version,attr"`
	Channel     Channel  `xml:"channel"`
	Description string   `xml:"description"`
	Title       string   `xml:"title"`
	Link        string   `xml:"link"`
}

type Channel struct {
	XMLName     xml.Name `xml:"channel"`
	Title       string   `xml:"title"`
	Link        string   `xml:"link"`
	Description string   `xml:"description"`
	Items       []Item   `xml:"item"`
}

type Item struct {
	XMLName xml.Name `xml:"item"`

	Title       string `xml:"title"`
	Link        string `xml:"link"`
	Duration    string `xml:"http://www.itunes.com/dtds/podcast-1.0.dtd duration, attr"`
	Description string `xml:"description"`
	PubDate     string `xml:"pubdate"`
	Guid        string `xml:"guid"`
}

// Podcast defines podcast
type Podcast struct {
	Name           string    `json:"name"`
	Description    string    `json:"description"`
	ListOfEpisodes []Episode `json:"list_of_episodes"`
}

// Episode defines episode
type Episode struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	URL         string `json:"url"`
	Duration    string `json:"duration"`
}

// PodcastsList defines podcast list
type PodcastsList struct {
	ListOfAllCollectedPodcasts []string  `json:"list_of_collected_podcasts"`
	ActualPodcastsAndEpisodes  []Podcast `json:"actual_podcasts_and_episodes"`
}

// IDs set IDs
func (s *GetPodcastRSSFeedService) FeedUrl(feedUrl string) *GetPodcastRSSFeedService {
	s.feedUrl = feedUrl
	return s
}
