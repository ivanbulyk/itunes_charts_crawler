package itunes

import (
	"context"
	"encoding/json"
)

// GetAdamIdsService get adamIds info
type GetAdamIdsService struct {
	c *Client
}

// Do send request
func (s *GetAdamIdsService) Do(ctx context.Context, opts ...RequestOption) (res AdamIds, err error) {
	r := &request{
		method:   "GET",
		endpoint: "/WebObjects/MZStore.woa/wa/viewTop?id=128&popId=3&genreId=26&selectedChartKind=podcast",
	}

	data, err := s.c.callAPI(ctx, r, opts...)
	if err != nil {
		return AdamIds{}, err
	}

	err = json.Unmarshal(data, &res)
	if err != nil {
		return AdamIds{}, err
	}
	return res, nil
}

// AdamIds
type AdamIds struct {
	PageData PageData `json:"pageData"`
}

// PageData
type PageData struct {
	SegmentedControl SegmentedControl `json:"segmentedControl"`
}

// SegmentedControl
type SegmentedControl struct {
	Segments []SomeInnerStruct `json:"segments"`
}

// SomeInnerStruct
type SomeInnerStruct struct {
	PageData PageDataInner `json:"pageData"`
}

// PageDataInner
type PageDataInner struct {
	TopCharts []SomeInnerStruct_2 `json:"topCharts"`
}

// SomeInnerStruct_2
type SomeInnerStruct_2 struct {
	AdamIds []string `json:"adamIds"`
}
