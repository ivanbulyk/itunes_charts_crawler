package itunes

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

// Endpoints
const (
	baseAPIMainURL = "https://itunes.apple.com"
)

const (
	timestampKey = "timestamp"
)

var (
	httpHeader                 http.Header
	fullURL                    string
	ListOfAllCollectedPodcasts []string
	apiKey                     = ""
	secretKey                  = ""
)

func currentTimestamp() int64 {
	return FormatTimestamp(time.Now())
}

// FormatTimestamp formats a time into Unix timestamp in milliseconds.
func FormatTimestamp(t time.Time) int64 {
	return t.UnixNano() / int64(time.Millisecond)
}

// getAPIEndpoint return the base endpoint of the Rest API
func getAPIEndpoint() string {
	return baseAPIMainURL
}

// NewClient initialize an API client instance with API key and secret key if necessary.
// You should always call this function before using this SDK.
// Services will be created by the form client.NewXXXService().
func NewClient(apiKey, secretKey string) *Client {
	return &Client{
		APIKey:     apiKey,
		SecretKey:  secretKey,
		BaseURL:    getAPIEndpoint(),
		UserAgent:  "iTunes/golang",
		HTTPClient: http.DefaultClient,
		Logger:     log.New(os.Stderr, "iTunes-golang ", log.LstdFlags),
	}
}

// NewGetAdamIdsService initialize GetAdamIdsService service
func (c *Client) NewGetAdamIdsService() *GetAdamIdsService {
	return &GetAdamIdsService{c: c}
}

// NewGetFeedURLService initialize GetFeedURLService service
func (c *Client) NewGetFeedURLService() *GetFeedURLService {
	return &GetFeedURLService{c: c}
}

// NewGetPodcastRSSFeedService initialize GetPodcastRSSFeedService service
func (c *Client) NewGetPodcastRSSFeedService() *GetPodcastRSSFeedService {
	return &GetPodcastRSSFeedService{c: c}
}

type doFunc func(req *http.Request) (*http.Response, error)

// Client define API client
type Client struct {
	APIKey     string
	SecretKey  string
	BaseURL    string
	UserAgent  string
	HTTPClient *http.Client
	Debug      bool
	Logger     *log.Logger
	TimeOffset int64
	do         doFunc
}

func (c *Client) debug(format string, v ...interface{}) {
	if c.Debug {
		c.Logger.Printf(format, v...)
	}
}

func (c *Client) parseRequest(r *request, opts ...RequestOption) (err error) {
	// set request options from user
	for _, opt := range opts {
		opt(r)
	}
	err = r.validate()
	if err != nil {
		return err
	}

	if strings.Contains(r.endpoint, "http") || strings.Contains(r.endpoint, "https") {
		fullURL = r.endpoint
	} else {
		fullURL = fmt.Sprintf("%s%s", c.BaseURL, r.endpoint)
	}

	queryString := r.query.Encode()
	body := &bytes.Buffer{}
	bodyString := r.form.Encode()
	header := http.Header{}
	if r.header != nil {
		header = r.header.Clone()
	}
	if bodyString != "" {
		header.Set("Content-Type", "application/x-www-form-urlencoded")
		body = bytes.NewBufferString(bodyString)
	}
	header.Set("Accept", "*/*")

	header.Set("X-Apple-Store-Front", "143441-1,30")

	if queryString != "" {
		fullURL = fmt.Sprintf("%s?%s", fullURL, queryString)
	}
	c.debug("full url: %s, body: %s", fullURL, bodyString)

	r.fullURL = fullURL
	r.header = header
	r.body = body
	return nil
}

func (c *Client) callAPI(ctx context.Context, r *request, opts ...RequestOption) (data []byte, err error) {
	err = c.parseRequest(r, opts...)
	if err != nil {
		return []byte{}, err
	}
	req, err := http.NewRequest(r.method, r.fullURL, r.body)
	if err != nil {
		return []byte{}, err
	}
	req = req.WithContext(ctx)
	req.Header = r.header
	c.debug("request: %#v", req)
	f := c.do
	if f == nil {
		f = c.HTTPClient.Do
	}
	res, err := f(req)
	httpHeader = res.Header
	if err != nil {
		return []byte{}, err
	}
	data, err = ioutil.ReadAll(res.Body)
	if err != nil {
		return []byte{}, err
	}
	defer func() {
		cerr := res.Body.Close()
		// Only overwrite the retured error if the original error was nil and an
		// error occurred while closing the body.
		if err == nil && cerr != nil {
			err = cerr
		}
	}()
	c.debug("response: %#v", res)
	c.debug("response body: %s", string(data))
	c.debug("response status code: %d", res.StatusCode)

	return data, nil
}

// GetAllData() gets all needed data
func GetAllData() (podcastsList PodcastsList) {
	ListOfAllCollectedPodcasts = []string{}

	client := NewClient(apiKey, secretKey)
	adamIds, err := client.NewGetAdamIdsService().Do(context.Background())
	if err != nil {
		fmt.Println(err)
		return
	}

	adamIdsSlice := adamIds.PageData.SegmentedControl.Segments[0].PageData.TopCharts[0].AdamIds
	feedUrl, err := client.NewGetFeedURLService().IDs(adamIdsSlice).Do(context.Background())
	if err != nil {
		fmt.Println(err)
		return
	}
	// fmt.Println(adamIdsSlice)
	// fmt.Println(feedUrl)

	feedUrlRSS := make([]string, 0)

	for _, itemRSS := range feedUrl.Results {
		feedUrlRSS = append(feedUrlRSS, itemRSS.FeedUrl)
	}
	feedUrlRSSlice := make([]Podcast, 0)
	for _, url := range feedUrlRSS {
		fmt.Println("The URL is:  ", url)
		feedUrlRssData, err := client.NewGetPodcastRSSFeedService().FeedUrl(url).Do(context.Background())
		if err != nil {
			fmt.Println(err)
			return
		}
		ListOfAllCollectedPodcasts = append(ListOfAllCollectedPodcasts, feedUrlRssData.Name)
		feedUrlRSSlice = append(feedUrlRSSlice, feedUrlRssData)

	}
	podcastsList.ListOfAllCollectedPodcasts = ListOfAllCollectedPodcasts
	podcastsList.ActualPodcastsAndEpisodes = feedUrlRSSlice

	return podcastsList
}
