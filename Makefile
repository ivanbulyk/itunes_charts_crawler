.PHONY: build start 

build:
	go build -v ./

start:

	go run cmd/*.go
	
.DEFAULT_GOAL := start