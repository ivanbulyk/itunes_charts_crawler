package handlers

import (
	"net/http"

	"gitlab.com/ivanbulyk/itunes_charts_crawler/internal/utils"
	"gitlab.com/ivanbulyk/itunes_charts_crawler/itunes"
)

func Top(w http.ResponseWriter, r *http.Request) {

	data := itunes.GetAllData()

	utils.RespondWithJSON(w, http.StatusOK, data)

}
