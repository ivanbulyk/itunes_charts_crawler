package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-chi/chi"
	"gitlab.com/ivanbulyk/itunes_charts_crawler/internal/handlers"
)

func main() {

	ctx := context.Background()

	r := chi.NewRouter()

	r.Get("/top", handlers.Top)

	srv := &http.Server{
		Addr:    ":8080",
		Handler: r,
	}
	defer srv.Close()

	stop := make(chan os.Signal, 1)
	signal.Notify(stop,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatal("start server failed: ", err)
		}

	}()
	log.Printf("Server started listening on port %v ..", srv.Addr)

	<-stop
	log.Println("caught stop signal")

	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("server shutdown failed")
	}

	log.Println("Server Exited Properly")

}
